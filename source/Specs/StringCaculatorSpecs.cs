 using Machine.Specifications;

namespace Specs
{
    using System;

    using developwithpassion.specifications.nsubstitue;

    public class StringCaculatorSpecs
    {
        public abstract class concern : Observes<StringCalculator.StringCalculator>
        {

        }

        [Subject(typeof(StringCalculator.StringCalculator))]
        public class when_given_an_empty_string : concern
        {
            private It should_return_zero = () => actual_calculated_result.ShouldEqual(0);

            private Because b = () => actual_calculated_result = sut.add(string.Empty);


            private static int actual_calculated_result; 
                   
        }

        [Subject(typeof(StringCalculator.StringCalculator))]
        public class when_given_a_single_string_number : concern
        {
            private It should_return_the_number = () => actual_calculated_result.ShouldEqual(expected_result);

            private Because b = () => actual_calculated_result = sut.add(input_value);

            Establish c = () =>
                {
                    expected_result = 5;
                    input_value = expected_result.ToString();
                };

            private static int actual_calculated_result;

            private static string input_value;

            private static int expected_result;
        }

        [Subject(typeof(StringCalculator.StringCalculator))]
        public class when_given_a_string_of_numbers_separated_by_a_comma : concern
        {
            private It should_return_the_sum_of_the_numbers = () => actual_calculated_result.ShouldEqual(expected_result);

            private Because b = () => actual_calculated_result = sut.add(input_value);

            Establish c = () =>
            {
                expected_result = 17;
                input_value = "7,3,7";
            };

            private static int actual_calculated_result;

            private static string input_value;

            private static int expected_result;
        }

        [Subject(typeof(StringCalculator.StringCalculator))]
        public class when_given_numbers_with_a_specified_delimter : concern
        {
            private It should_return_the_sum_of_the_numbers_delimited_by_specified_delimiter = () => actual_calculated_result.ShouldEqual(expected_result);

            private Because b = () => actual_calculated_result = sut.add(input_value);

            Establish c = () =>
            {
                expected_result = 17;
                input_value = "//;\n7;3;7";
            };

            private static int actual_calculated_result;

            private static string input_value;

            private static int expected_result;
        }

        [Subject(typeof(StringCalculator.StringCalculator))]
        public class when_given_postive_numbers_and_negative_numbers : concern
        {
            private It should_throw_an_exception = () => typeof(Exception).ShouldBeThrownBy(()=>sut.add(input_value));

            Establish c = () =>
            {
                expected_result = 10;
                input_value = "//;\n-7;3;6";
            };

            private static int actual_calculated_result;

            private static string input_value;

            private static int expected_result;
        }
    }
}
