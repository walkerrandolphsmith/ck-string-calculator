namespace StringCalculator
{
    //fuck your changes. 
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class StringCalculator
    {
        public int add(string string_of_numbers)
        {
           var delimiters = get_delimiters(string_of_numbers);
            string_of_numbers = strip_delimiters(string_of_numbers);

            return add_string_numbers(delimiters, string_of_numbers);
        }

        private string strip_delimiters(string original_string)
        {
            if (original_string.StartsWith("//"))
            {
               return original_string.Substring(3);
            }
            return original_string;
        }

        private int add_string_numbers(IEnumerable<char> delimiters, string string_of_numbers)
        {
            var negative_numbers = new List<int>();
            var string_numbers = string_of_numbers.Split(delimiters.ToArray());
            var calculated_output = 0;
            foreach (var string_number in string_numbers)
            {
                var number = 0;
                int.TryParse(string_number, out number);
                if (number < 0)
                {
                    negative_numbers.Add(number);
                }
                calculated_output += number;
            }
            if (negative_numbers.Any())
            {
                var exception_message = "negatives not allowed: ";
                foreach (var number in negative_numbers)
                {
                    exception_message += number.ToString() + " ";
                }
                throw new Exception(exception_message);
            }
            return calculated_output;
        }

        private IEnumerable<char> get_delimiters(string original_string)
        {
            var delimiters = new List<char>() { ',', '\n' };
            if (original_string.StartsWith("//"))
            {
                delimiters.Add(original_string[2]);
            }
            return delimiters;
        }
    }
}
